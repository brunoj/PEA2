//
// Created by brunoj on 25.11.17.
//

#ifndef PEA2_TABUSEARCH_H
#define PEA2_TABUSEARCH_H
#include <vector>
#include <algorithm>
#include <time.h>
#include "Graph.h"

class TabuSearch {
private:
    Graph *graph;
    vector<int> wynik;
    vector<vector<int>> tabuList;

public:
    int numberOfIter = 3000;
    TabuSearch(Graph *graph);
    void generatePerm();
    int getCost(vector<int> wynik);
    void zmiana(int od1,int od2);
    void getRoute();
    void tabuMove(int od1,int od2, int w);
    void decMove();
    void printRoute(vector<int> wynik);
    long long getRouteExp();
    long long getRouteExpPer( int number);


};


#endif //PEA2_TABUSEARCH_H
