//
// Created by brunoj on 25.11.17.
//

#include <values.h>
#include <limits.h>
#include <chrono>
#include "TabuSearch.h"

#define TIME_TRY 500

TabuSearch::TabuSearch(Graph *graph) {
    this->graph=graph;
    this->wynik.resize(this->graph->getV()+1);
    this->tabuList.resize(this->graph->getV());
    for(int i=0; i<this->graph->getV();i++){
       tabuList[i].resize(this->graph->getV());
    }
    for(int i=0;i<this->graph->getV();i++)
    {
        for(int j=0;j<graph->getV();j++){
            this->tabuList[i][j]=0;
        }
    }
}
void TabuSearch::printRoute(vector<int> wynik)
{
    for(int i=0;i<this->graph->getV();i++)
    {
        int start=wynik[i];
        int end = wynik[i+1];
        std::cout<<start<<" --> "<<end;
    }
}
void TabuSearch::generatePerm()
{
    auto rng = std::default_random_engine {};
    int iloscMiast = this->graph->getV();
    vector<int> tmp;
    for(int i=1;i<iloscMiast;i++)
    {
        tmp.push_back(i);

    }


    std::shuffle(tmp.begin(), tmp.end(),rng);
    tmp.push_back(0);
    tmp.insert(tmp.begin(),0);
    this->wynik=tmp;
    for(int i=0;i<tmp.size();i++)
    {
        std::cout<<this->wynik[i]<<"\n";
    }

}
int TabuSearch::getCost( vector<int> wynik)
{
    int cost = 0;
    for(int i=0;i<this->graph->getV();i++)
    {
        int start=wynik[i];
        int end = wynik[i+1];
        cost=cost+graph->odleglosc[start][end];
    }
    return cost;

}
void TabuSearch::zmiana(int id1,int id2)
{
    int tmp = this->wynik[id1];
    this->wynik[id1]=this->wynik[id2];
    this->wynik[id2]=tmp;
}
void TabuSearch::tabuMove(int od1, int od2, int w)
{
    this->tabuList[od1][od2]+=w;
    this->tabuList[od2][od1]+=w;

}
void TabuSearch::decMove()
{
    for(int i=0;i<graph->getV();i++)
    {
        for(int j=0;j<graph->getV();j++){
            if(this->tabuList[i][j]>0)
            {
                this->tabuList[i][j]= this->tabuList[i][j]-1;
            }
        }
    }
}
void TabuSearch::getRoute()
{
    long long  stop = 1000000000000;
    auto start = chrono::system_clock::now();
    generatePerm();
    int countTime = 0;
    vector<int> bestBestSol;
    int bestBestCost = getCost(this->wynik);
    for(int iter=0;iter<numberOfIter;iter++) {
        int city1 = 0;
        int city2 = 0;
        int bestCost = INT_MAX;
        for (int i = 1; i < this->graph->getV(); i++) {
            for (int j = 2; j < this->graph->getV(); j++) {
                if (i == j)
                    continue;
                vector<int> newWynik;
                newWynik.resize(this->wynik.size());
                newWynik = this->wynik;
                iter_swap(newWynik.begin() + i, newWynik.begin() + j);
                int newCost = getCost(newWynik);
                if (newCost < bestCost && tabuList[i][j] == 0) {
                    city1 = i;
                    city2 = j;
                    bestCost = newCost;
                }

            }
        }
        if (city1 != 0)
        {
            decMove();
            tabuMove(city1, city2, 5);
        }
        if(bestBestCost>bestCost)
        {
            bestBestCost = bestCost;
            bestBestSol = this->wynik;
        }
        iter_swap(wynik.begin()+city1,wynik.begin()+city2);
        auto end = chrono::system_clock::now();
        auto elapsed = chrono::duration_cast<chrono::seconds>(end - start);
        if(elapsed.count()>stop)
            break;

    }
    std::cout<<"wynik to "<<bestBestCost;
    std::cout<<"droga to ";
    printRoute(bestBestSol);
}

long long TabuSearch::getRouteExp()
{

    auto start = chrono::system_clock::now();
    generatePerm();
    int countTime = 0;
    vector<int> bestBestSol;
    int bestBestCost = getCost(this->wynik);
    for(int iter=0;iter<numberOfIter;iter++) {
        int city1 = 0;
        int city2 = 0;
        int bestCost = INT_MAX;
        for (int i = 1; i < this->graph->getV(); i++) {
            for (int j = 2; j < this->graph->getV(); j++) {
                if (i == j)
                    continue;
                vector<int> newWynik;
                newWynik.resize(this->wynik.size());
                newWynik = this->wynik;
                iter_swap(newWynik.begin() + i, newWynik.begin() + j);
                int newCost = getCost(newWynik);
                if (newCost < bestCost && tabuList[i][j] == 0) {
                    city1 = i;
                    city2 = j;
                    bestCost = newCost;
                }

            }
        }
        if (city1 != 0)
        {
            decMove();
            tabuMove(city1, city2, 5);
        }
        if(bestBestCost>bestCost)
        {
            bestBestCost = bestCost;
            bestBestSol = this->wynik;
        }
        iter_swap(wynik.begin()+city1,wynik.begin()+city2);
    }
    auto end = chrono::system_clock::now();
    auto elapsed = chrono::duration_cast<chrono::milliseconds>(end - start);

    return elapsed.count();
}
long long TabuSearch::getRouteExpPer(int number)
{
    int bestSolutionScore;
    auto start = chrono::system_clock::now();
    generatePerm();
    int countTime = 0;
    vector<int> bestBestSol;
    int bestBestCost = getCost(this->wynik);
    for(int iter=0;iter<numberOfIter;iter++) {
        int city1 = 0;
        int city2 = 0;
        int bestCost = INT_MAX;
        for (int i = 1; i < this->graph->getV(); i++) {
            for (int j = 2; j < this->graph->getV(); j++) {
                if (i == j)
                    continue;
                vector<int> newWynik;
                newWynik.resize(this->wynik.size());
                newWynik = this->wynik;
                iter_swap(newWynik.begin() + i, newWynik.begin() + j);
                int newCost = getCost(newWynik);
                if (newCost < bestCost && tabuList[i][j] == 0) {
                    city1 = i;
                    city2 = j;
                    bestCost = newCost;
                }

            }
        }
        if (city1 != 0)
        {
            decMove();
            tabuMove(city1, city2, 500);
        }
        if(bestBestCost>bestCost)
        {
            bestBestCost = bestCost;
            bestBestSol = this->wynik;
        }
        iter_swap(wynik.begin()+city1,wynik.begin()+city2);
    }
    auto end = chrono::system_clock::now();
    auto elapsed = chrono::duration_cast<chrono::milliseconds>(end - start);
    switch (number)
    {
        case 0:
            bestSolutionScore =39;
            break;
        case 1:
            bestSolutionScore = 6905;
            break;
        case 2:
            bestSolutionScore =36230;
            break;
        default:
            break;
    }
    float  c= abs(bestSolutionScore-bestBestCost);
    float a = c/bestSolutionScore;
    cout<<a*100<<"%\n";
    cout<<"wynik: "<<bestBestCost<<"\n";
    cout<<"czas: "<<elapsed.count();
    return elapsed.count();
}
